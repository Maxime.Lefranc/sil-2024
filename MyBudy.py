import tkinter as tk
import customtkinter
from PIL import Image
import random
import json
import datetime

class Entity():

    def __init__(self, name):
        self.name = name
        self.members_state = dict()
        self.init_members_states()
    
    def init_members_states(self):
        members = ["tete", "cou", "epaule_l","epaule_r", "pec_l", "pec_r","ventre","bras_l", "bras_r","avant-bras_l", "avant-bras_r","main_l", "main_r","cuisse_l", "cuisse_r", "genou_l","genou_r", "jambe_l","jambe_r", "pied_l", "pied_r"]
        for member in members:
            self.members_state[member] = True

    def display(self):
        print(f"Nom : {self.name}")
        for member in self.members_state:
            print(f"{member} : {self.members_state[member]}")

class App(customtkinter.CTk):

    def __init__(self, user):
        super().__init__()
        # parameters
        self.title("My Budy")
        self.pagenum = 1
        self.user = user
        self.propositions = self._getPropositions()
        print("self.propositions = ", self.propositions)
        self.trainings = {}
        names = ["self.one_train_empty", "self.one_train_empty2", "self.one_train_empty3", "self.one_sugg_empty", "self.one_sugg_empty2", "self.one_oth_empty", "self.one_oth_empty2", "self.one_oth_empty3"]
        for name in names:
            self.trainings[name] = ["----------------------------------------------------------------------------------------------", " --/-- "]
        self.current_training = []
        #
        self.after(250, lambda: self.iconbitmap('imgs/bitmap.ico'))
        self.geometry("400x530")
        self.resizable(False, False)
        self.configure(fg="white",bg="white")
        # ROWS
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        self.grid_rowconfigure(3, weight=1)
        self.grid_rowconfigure(4, weight=1)
        self.grid_rowconfigure(5, weight=1)
        self.grid_rowconfigure(6, weight=1)
        self.grid_rowconfigure(7, weight=1)
        self.grid_rowconfigure(8, weight=1)
        self.grid_rowconfigure(9, weight=1)
        self.grid_rowconfigure(10, weight=1)
        self.grid_rowconfigure(11, weight=1)
        self.grid_rowconfigure(12, weight=1)
        self.grid_rowconfigure(13, weight=1)
        self.grid_rowconfigure(14, weight=1)
        self.grid_rowconfigure(15, weight=1)
        self.grid_rowconfigure(16, weight=1)
        self.grid_rowconfigure(17, weight=1)
        self.grid_rowconfigure(18, weight=1)
        self.grid_rowconfigure(19, weight=1)
        self.grid_rowconfigure(20, weight=1)
        self.grid_rowconfigure(21, weight=1)
        self.grid_rowconfigure(22, weight=1)
        self.grid_rowconfigure(23, weight=1)
        self.grid_rowconfigure(24, weight=1)
        self.grid_rowconfigure(25, weight=1)
        self.grid_rowconfigure(26, weight=1)
        self.grid_rowconfigure(27, weight=1)
        self.grid_rowconfigure(28, weight=1)
        self.grid_rowconfigure(29, weight=1)
        self.grid_rowconfigure(30, weight=1)
        self.grid_rowconfigure(31, weight=1)
        self.grid_rowconfigure(32, weight=1)
        self.grid_rowconfigure(33, weight=1)
        self.grid_rowconfigure(34, weight=1)
        self.grid_rowconfigure(35, weight=1)
        self.grid_rowconfigure(36, weight=1)
        self.grid_rowconfigure(37, weight=1)
        self.grid_rowconfigure(38, weight=1)
        self.grid_rowconfigure(39, weight=1)
        self.grid_rowconfigure(40, weight=1)
        # COLUMS
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)
        self.grid_columnconfigure(4, weight=1)
        self.grid_columnconfigure(5, weight=1)
        self.grid_columnconfigure(6, weight=1)
        # UI
        self._initUIPrincipal()
    
    def _getPropositions(self):
        with open('training_programs/training.json', encoding="utf8") as json_data:
            data_dict = json.load(json_data)
            print(data_dict)
        return data_dict

    # Init pages

    def _initUIPrincipal(self):
        self._switchToPagePrincipale()
        self._define_menubarlike_principale()
        self._define_centralWidget()
        self._define_footWidget()
    
    def _initUITraining(self):
        self._switchToPageTraining()
        self._define_menubarlike_training()
    
    def _initUITrainingOnePage(self, name):
        self._switchToPageTraining()
        self._define_menubarlike_training_details(name)

    def _define_menubarlike_principale(self):
        # My_account
        self.home_image = customtkinter.CTkImage(Image.open("icons/my_account.png"), size=(26, 26))
        self.home_button = customtkinter.CTkButton(self, corner_radius=0, height=30, width=30, border_spacing=0, text="",
                                                   fg_color="white", text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),
                                                   image=self.home_image, command=self.debug)
        self.home_button.grid(row=0, column=0, pady=(10,0), padx=(10,0),  sticky = "NW")
        #Settings
        self.settings_image = customtkinter.CTkImage(Image.open("icons/settings.png"), size=(26, 26))
        self.settings_button = customtkinter.CTkButton(self, corner_radius=0, height=20, width=30, border_spacing=0, text="",
                                                   fg_color="white", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                   image=self.settings_image)
        self.settings_button.grid(row=0, column=6, pady=(10,0), padx=(0,10), sticky = "NE")
        # DATES
        self.j_7_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Lun\n20",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_6_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Auj\n21",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white", fg_color="green")
        self.j_5_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Mer\n22",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_4_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Jeu\n23",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_3_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Ven\n24",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_2_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Sam\n25",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_1_button = customtkinter.CTkButton(self, corner_radius=10, border_spacing=0, text="Dim\n26",
                                                   text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),bg_color="white")
        self.j_7_button.grid(row=1, column=0, padx=5, pady=10, sticky = "nswe")
        self.j_6_button.grid(row=1, column=1, padx=5, pady=10, sticky = "nswe")
        self.j_5_button.grid(row=1, column=2, padx=5, pady=10, sticky = "nswe")
        self.j_4_button.grid(row=1, column=3, padx=5, pady=10, sticky = "nswe")
        self.j_3_button.grid(row=1, column=4, padx=5, pady=10, sticky = "nswe")
        self.j_2_button.grid(row=1, column=5, padx=5, pady=10, sticky = "nswe")
        self.j_1_button.grid(row=1, column=6, padx=5, pady=10, sticky = "nswe")

    def _define_menubarlike_training(self):
        # Label principal
        self.training_label = customtkinter.CTkLabel(self, text="Propositions d'entrainements", fg_color="white", bg_color="black", font=("arial",19),text_color=("gray10", "gray10"))
        self.training_label.grid(row=0, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Back to home
        self.back_image = customtkinter.CTkImage(Image.open("icons/back.png"), size=(26, 26))
        self.back_button = customtkinter.CTkButton(self, corner_radius=0, height=30, width=30, border_spacing=0, text="",
                                                   fg_color="white", text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),
                                                   image=self.back_image, command=lambda : self.changepage(1, name=None))
        self.back_button.grid(row=1, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Mise en page
        self.training_label = customtkinter.CTkLabel(self, text="Entrainements", fg_color="white", font=("arial",17),text_color=("gray10", "gray10"))
        self.training_label.grid(row=2, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Ligne type case : reload line date +
        self.one_train_coche = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_train_coche.grid(row=3, column=0, pady=1, padx=10,sticky = "NW")
        self.one_train_reload_image = customtkinter.CTkImage(Image.open("icons/reload2.png"), size=(20, 20))
        self.one_train_reload = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=0))
        self.one_train_reload.grid(row=3, column=0, pady=0, padx=34,sticky = "NW")
        # self.one_train_empty_image = customtkinter.CTkImage(Image.open("icons/line.png"), size=(20, 20))
        self.one_train_empty = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_train_empty"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_train_empty"][0]))
        self.one_train_empty.grid(row=3, column=0, pady=3, padx=100, sticky="NW")
        self.one_train_date = customtkinter.CTkLabel(self, text=self.trainings["self.one_train_empty"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_train_date.grid(row=3, column=0, pady=0, padx=60, sticky="NW")
        # training 2
        self.one_train_coche2 = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_train_coche2.grid(row=4, column=0, pady=1, padx=10,sticky = "NW")
        self.one_train_reload2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=1))
        self.one_train_reload2.grid(row=4, column=0, pady=0, padx=34,sticky = "NW")

        self.one_train_empty2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_train_empty2"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_train_empty2"][0]))
        self.one_train_empty2.grid(row=4, column=0, pady=3, padx=100, sticky="NW")
        self.one_train_date2 = customtkinter.CTkLabel(self, text=self.trainings["self.one_train_empty2"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_train_date2.grid(row=4, column=0, pady=0, padx=60, sticky="NW")
        # training 3
        self.one_train_coche3 = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_train_coche3.grid(row=5, column=0, pady=1, padx=10,sticky = "NW")
        self.one_train_reload3 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=2))
        self.one_train_reload3.grid(row=5, column=0, pady=0, padx=34,sticky = "NW")

        self.one_train_empty3 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_train_empty3"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_train_empty3"][0]))
        self.one_train_empty3.grid(row=5, column=0, pady=3, padx=100, sticky="NW")
        self.one_train_date3 = customtkinter.CTkLabel(self, text=self.trainings["self.one_train_empty3"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_train_date3.grid(row=5, column=0, pady=0, padx=60, sticky="NW")
        # Mise en page
        self.training_label = customtkinter.CTkLabel(self, text="Suggestions avec nouveau matériel", fg_color="white", font=("arial",17), text_color=("gray10", "gray10"))
        self.training_label.grid(row=7, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # suggestion 1
        self.one_sugg_coche = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_sugg_coche.grid(row=8, column=0, pady=1, padx=10,sticky = "NW")
        self.one_sugg_reload = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=3))
        self.one_sugg_reload.grid(row=8, column=0, pady=0, padx=34,sticky = "NW")

        self.one_sugg_empty = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_sugg_empty"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_sugg_empty"][0]))
        self.one_sugg_empty.grid(row=8, column=0, pady=3, padx=100, sticky="NW")
        self.one_sugg_date = customtkinter.CTkLabel(self, text=self.trainings["self.one_sugg_empty"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_sugg_date.grid(row=8, column=0, pady=0, padx=60, sticky="NW")
        # suggestion 2
        self.one_sugg_coche2 = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_sugg_coche2.grid(row=9, column=0, pady=1, padx=10,sticky = "NW")
        self.one_sugg_reload2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=4))
        self.one_sugg_reload2.grid(row=9, column=0, pady=0, padx=34,sticky = "NW")

        self.one_sugg_empty2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_sugg_empty2"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_sugg_empty2"][0]))
        self.one_sugg_empty2.grid(row=9, column=0, pady=3, padx=100, sticky="NW")
        self.one_sugg_date2 = customtkinter.CTkLabel(self, text=self.trainings["self.one_sugg_empty2"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_sugg_date2.grid(row=9, column=0, pady=0, padx=60, sticky="NW")
        # Autres
        self.training_label = customtkinter.CTkLabel(self, text="Autres", fg_color="white", font=("arial",18), text_color=("gray10", "gray10"))
        self.training_label.grid(row=11, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # autre 1
        self.one_oth_coche = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_oth_coche.grid(row=12, column=0, pady=1, padx=10,sticky = "NW")
        self.one_oth_reload = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=5))
        self.one_oth_reload.grid(row=12, column=0, pady=0, padx=34,sticky = "NW")

        self.one_oth_empty = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_oth_empty"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_oth_empty"][0]))
        self.one_oth_empty.grid(row=12, column=0, pady=3, padx=100, sticky="NW")
        self.one_oth_date = customtkinter.CTkLabel(self, text=self.trainings["self.one_oth_empty"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_oth_date.grid(row=12, column=0, pady=0, padx=60, sticky="NW")
        # autre 2
        self.one_oth_coche2 = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_oth_coche2.grid(row=13, column=0, pady=1, padx=10,sticky = "NW")
        self.one_oth_reload2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=6))
        self.one_oth_reload2.grid(row=13, column=0, pady=0, padx=34,sticky = "NW")

        self.one_oth_empty2 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_oth_empty2"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_oth_empty2"][0]))
        self.one_oth_empty2.grid(row=13, column=0, pady=3, padx=100, sticky="NW")
        self.one_oth_date2 = customtkinter.CTkLabel(self, text=self.trainings["self.one_oth_empty2"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_oth_date2.grid(row=13, column=0, pady=0, padx=60, sticky="NW")
        # autre 3
        self.one_oth_coche3 = customtkinter.CTkButton(self, height=24, width=24, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", border_color="black", border_width=2)
        self.one_oth_coche3.grid(row=14, column=0, pady=1, padx=10,sticky = "NW")
        self.one_oth_reload3 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", image=self.one_train_reload_image, command=lambda : self.training_proposition(row=7))
        self.one_oth_reload3.grid(row=14, column=0, pady=0, padx=34,sticky = "NW")

        self.one_oth_empty3 = customtkinter.CTkButton(self,height=1, width=1, border_spacing=0, text=self.trainings["self.one_oth_empty3"][0],
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="white"
                                                    ,fg_color="white", command=lambda : self.changepage(3, name=self.trainings["self.one_oth_empty3"][0]))
        self.one_oth_empty3.grid(row=14, column=0, pady=3, padx=100, sticky="NW")
        self.one_oth_date3 = customtkinter.CTkLabel(self, text=self.trainings["self.one_oth_empty3"][1], fg_color="white", font=("arial",14), text_color=("gray10", "gray10"))
        self.one_oth_date3.grid(row=14, column=0, pady=0, padx=60, sticky="NW")

    def _define_menubarlike_training_details(self, name):
        # Label principal
        for train in self.propositions:
            if train["name"] == name:
                training = train
        self.training_label = customtkinter.CTkLabel(self, text="Propositions d'entrainements", fg_color="white", font=("arial",20), text_color=("gray10", "gray10"))
        self.training_label.grid(row=0, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Back to home
        self.back_image = customtkinter.CTkImage(Image.open("icons/back.png"), size=(26, 26))
        self.back_button = customtkinter.CTkButton(self, corner_radius=0, height=30, width=30, border_spacing=0, text="",
                                                   fg_color="white", text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),
                                                   image=self.back_image, command=lambda : self.changepage(2, name=None))
        self.back_button.grid(row=1, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Mise en page
        self.training_label = customtkinter.CTkLabel(self, text="Entrainements", fg_color="white", font=("arial",20), text_color=("gray10", "gray10"))
        self.training_label.grid(row=2, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
        # Details
        if name == "----------------------------------------------------------------------------------------------":
            return -1
        name = customtkinter.CTkLabel(self, text=training["name"], fg_color="white", font=("Segoe UI Semibold",16), text_color=("gray10", "gray10"))
        name.grid(row=3, column=0, pady=(0,0), padx=(10,0),  sticky = "NW", columnspan=2)
        intensity = customtkinter.CTkLabel(self, text="Intensité : " + training["Intensity"], fg_color="white", font=("arial",13), text_color=("gray10", "gray10"), width=1)
        intensity.grid(row=4, column=0, pady=(0,0), padx=(10,0),  sticky = "NW",columnspan=2)
        nb_sessions = customtkinter.CTkLabel(self, text="Nombre de sessions : " + str(training["nb_of_sessions"]), fg_color="white", font=("arial",13), text_color=("gray10", "gray10"), width=1)
        nb_sessions.grid(row=5, column=0, pady=(0,0), padx=(10,0),  sticky = "NW",columnspan=2)
        sessions = customtkinter.CTkLabel(self, text="Session.s : ", fg_color="white", font=("Segoe UI Semibold",16), text_color=("gray10", "gray10"), width=1)
        sessions.grid(row=6, column=0, pady=(0,0), padx=(10,0),  sticky = "NW",columnspan=2)
        i = 1
        for element in training["Sessions"]:
            sessions_element = customtkinter.CTkLabel(self, text=element[0], fg_color="white", font=("arial",13), text_color=("gray10", "gray10"), width=1)
            sessions_element.grid(row=6+i, column=0, pady=(0,0), padx=(10,0),  sticky = "NW")
            i+=1
            
    def _define_centralWidget(self):
        # Memory
        print("self.user.members_state = ", self.user.members_state)
        names_buttons = {"tete":"head_button", "cou":"neck_button", 
                         "epaule_l":"shoulderleft_button", "epaule_r":"shoulderright_button", 
                         "pec_l":"pecleft_button", "pec_r":"pecright_button", 
                         "bras_l":"armeft_button", "bras_r":"armright_button",
                         "avant-bras_l":"forearmeft_button", "avant-bras_r":"forearmright_button",
                         "ventre":"navel_button",
                         "main_l":"handleft_button", "main_r":"handright_button",
                         "cuisse_l":"thighleft_button", "cuisse_r":"thighright_button",
                         "genou_l":"kneesleft_button", "genou_r":"kneesright_button",
                         "jambe_l":"legleft_button", "jambe_r":"legright_button",
                         "pied_l":"footleft_button", "pied_r":"footright_button",
                         }
        # HEAD
        self.head_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.head_button.configure(command= lambda : self.changeStateMembers("head_button","tete", memory=False))
        self.head_button.grid(row=4, column=2, pady=4, padx=5,sticky = "NW")
        # NECK
        self.neck_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.neck_button.configure(command= lambda : self.changeStateMembers("neck_button","cou", memory=False))
        self.neck_button.grid(row=6, column=2, pady=5, padx=5,sticky = "NW")
        
        # # # SHOULDER
        self.shoulderleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.shoulderright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.shoulderleft_button.configure(command= lambda : self.changeStateMembers("shoulderleft_button","epaule_l", memory=False))
        self.shoulderright_button.configure(command= lambda : self.changeStateMembers("shoulderright_button","epaule_r", memory=False))
        self.shoulderleft_button.grid(row=6, column=1, pady=20, padx=10,sticky = "N")
        self.shoulderright_button.grid(row=6, column=2, pady=(20,0), padx=(0,8),sticky = "NE")
        # # # PECs
        self.pecleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.pecright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.pecleft_button.configure(command= lambda : self.changeStateMembers("pecleft_button","pec_l", memory=False))
        self.pecright_button.configure(command= lambda : self.changeStateMembers("pecright_button","pec_r", memory=False))
        self.pecleft_button.grid(row=6, column=1, pady=(0,0), padx=(0,0),sticky = "SE")
        self.pecright_button.grid(row=6, column=2, pady=(0,0), padx=(0,0),sticky = "S")
        # # # ARM
        self.armeft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.armright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.armeft_button.configure(command= lambda : self.changeStateMembers("armeft_button","bras_l", memory=False))
        self.armright_button.configure(command= lambda : self.changeStateMembers("armright_button","bras_r", memory=False))
        self.armeft_button.grid(row=7, column=1, pady=(0,0), padx=(0,0),sticky = "S")
        self.armright_button.grid(row=7, column=2, pady=(0,0), padx=(0,5),sticky = "SE")
        # # # ELBOW
        # self.elbeft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
        #                                             text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent")
        # self.elbright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
        #                                             text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent")
        # self.elbeft_button.grid(row=8, column=4, pady=(0,0), padx=(0,0))
        # self.elbright_button.grid(row=7, column=5, pady=(0,0), padx=(0,0), sticky="E")
        # # FOREARM
        self.forearmeft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.forearmright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.forearmeft_button.configure(command= lambda : self.changeStateMembers("forearmeft_button","avant-bras_l", memory=False))
        self.forearmright_button.configure(command= lambda : self.changeStateMembers("forearmright_button","avant-bras_r", memory=False))
        self.forearmeft_button.grid(row=10, column=1, pady=(0,0), padx=(0,10))
        self.forearmright_button.grid(row=10, column=2, pady=(0,0), padx=(35,0),sticky = "E")
        # # NAVEL
        self.navel_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.navel_button.configure(command= lambda : self.changeStateMembers("navel_button","ventre", memory=False))
        self.navel_button.grid(row=10, column=2, pady=(0,0), padx=(7,0), sticky = "NW")
        # # HAND
        self.handleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.handright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.handleft_button.configure(command= lambda : self.changeStateMembers("handleft_button","main_l", memory=False))
        self.handright_button.configure(command= lambda : self.changeStateMembers("handright_button","main_r", memory=False))
        self.handleft_button.grid(row=14, column=1, pady=(0,0), padx=(7,0), sticky = "NW")
        self.handright_button.grid(row=14, column=3, pady=(0,0), padx=(7,0), sticky = "NW")
        # # thigh
        self.thighleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.thighright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.thighleft_button.configure(command= lambda : self.changeStateMembers("thighleft_button","cuisse_l", memory=False))
        self.thighright_button.configure(command= lambda : self.changeStateMembers("thighright_button","cuisse_r", memory=False))
        self.thighleft_button.grid(row=17, column=1, pady=(0,0), padx=(0,0),sticky = "NE")
        self.thighright_button.grid(row=17, column=2, pady=(0,0), padx=(0,0),sticky = "N")
        # # knees
        self.kneesleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.kneesright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.kneesleft_button.configure(command= lambda : self.changeStateMembers("kneesleft_button","genou_l", memory=False))
        self.kneesright_button.configure(command= lambda : self.changeStateMembers("kneesright_button","genou_r", memory=False))
        self.kneesleft_button.grid(row=22, column=1, pady=(0,0), padx=(0,0),sticky = "SE")
        self.kneesright_button.grid(row=22, column=2, pady=(0,0), padx=(0,0),sticky = "S")
        # # leg
        self.legleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.legright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.legleft_button.configure(command= lambda : self.changeStateMembers("legleft_button","jambe_l", memory=False))
        self.legright_button.configure(command= lambda : self.changeStateMembers("legright_button","jambe_r", memory=False))
        self.legleft_button.grid(row=27, column=1, pady=(0,0), padx=(0,0),sticky = "E")
        self.legright_button.grid(row=27, column=2, pady=(0,0), padx=(0,0))
        # # foot
        self.footleft_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.footright_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.footleft_button.configure(command= lambda : self.changeStateMembers("footleft_button","pied_l", memory=False))
        self.footright_button.configure(command= lambda : self.changeStateMembers("footright_button","pied_r", memory=False))
        self.footleft_button.grid(row=34, column=1, pady=(0,0), padx=(0,0),sticky = "E")
        self.footright_button.grid(row=34, column=2, pady=(0,0), padx=(0,0))
        # Memomy
        for element in self.user.members_state:
            if self.user.members_state[element] == False:
                self.changeStateMembers(names_buttons[element], element, memory=True)

    def _define_footWidget(self):
        # Objectifs
        trophy_image = customtkinter.CTkImage(Image.open("icons/trophy.png"), size=(35, 35))
        self.trophy_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10,border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="#FE9900",
                                                    image=trophy_image, fg_color="#FE9900")
        self.trophy_button.grid(row=42, column=0, pady=5, padx=0, sticky="N",columnspan=2)
        # # Trainings
        training_image = customtkinter.CTkImage(Image.open("icons/trainings.png"), size=(35, 35))
        self.training_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="#FE9900",
                                                    image=training_image, fg_color='#FE9900', command=lambda : self.changepage(2, name=None))
        self.training_button.grid(row=42, column=1, pady=5, padx=0, sticky="N",columnspan=2)
        # # Food
        food_image = customtkinter.CTkImage(Image.open("icons/apple.png"), size=(35, 35))
        self.food_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="#FE9900",
                                                    image=food_image, fg_color='#FE9900')
        self.food_button.grid(row=42, column=4, pady=5, padx=0, sticky="N",columnspan=2)
        # # Community
        com_image = customtkinter.CTkImage(Image.open("icons/com.png"), size=(35, 35))
        self.com_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="#FE9900",
                                                    image=com_image, fg_color="#FE9900")
        self.com_button.grid(row=42, column=5, pady=5, padx=0, sticky="N",columnspan=2)

    def _switchToPagePrincipale(self):
        page = tk.Frame(self, height=400, width=570)
        page.grid()
        tk.Label(page, text = 'This is page 1').grid(row = 0)
        # tk.Button(page, text = 'To page 2', command = self.changepage).grid(row = 1)
        # Background
        background_image = customtkinter.CTkImage(Image.open("imgs/body_rvv2_less.jpg"), size=(400, 550))
        bg_lbl = customtkinter.CTkLabel(self, text="", image=background_image)
        bg_lbl.place(x=0, y=-17)
        # HEAD
        self.head_button = customtkinter.CTkButton(self, corner_radius=10, height=10, width=10, border_spacing=0, text="",
                                                    text_color=("gray10", "gray10"), hover_color=("gray70", "gray30"),bg_color="transparent"
                                                    ,fg_color="green")
        self.head_button.configure(command= lambda : self.changeStateMembers("head_button","tete"))
        self.head_button.grid(row=4, column=2, pady=4, padx=5,sticky = "NW")
    
    def _switchToPageTraining(self):
        page = tk.Frame(self, height=400, width=570, bg="white")
        page.grid()
        tk.Label(page, text = 'This is page 1').grid(row = 0)
        # tk.Button(page, text = 'To page 2', command = self.changepage).grid(row = 1)
        # Background
        background_image = customtkinter.CTkImage(Image.open("imgs/white.jpg"), size=(400, 600))
        bg_lbl = customtkinter.CTkLabel(self, text="", image=background_image)
        bg_lbl.place(x=0, y=0)
    
    def changepage(self, num, name):
        for widget in self.winfo_children():
            widget.destroy()
        if num == 2:
            self._initUITraining()
            self.pagenum = 2
        elif num == 1:
            self._initUIPrincipal()
            self.pagenum = 1
        else:
            self._initUITrainingOnePage(name)
            self.pagenum = 3

    def changeStateMembers(self, name_button, member, memory):
        if memory == False:
            self.user.members_state[member] = not(self.user.members_state[member])
        if self.user.members_state[member] == False:
            exec(f'self.{name_button}.configure(fg_color="red")')
        else:
            exec(f'self.{name_button}.configure(fg_color="green")')

    def training_proposition(self, row):
        names_but = [self.one_train_empty, self.one_train_empty2, self.one_train_empty3, self.one_sugg_empty, self.one_sugg_empty2, self.one_oth_empty, self.one_oth_empty2, self.one_oth_empty3]
        names_str = ["self.one_train_empty", "self.one_train_empty2", "self.one_train_empty3", "self.one_sugg_empty", "self.one_sugg_empty2", "self.one_oth_empty", "self.one_oth_empty2", "self.one_oth_empty3"]
        dates = [self.one_train_date, self.one_train_date2, self.one_train_date3, self.one_sugg_date, self.one_sugg_date2, self.one_oth_date, self.one_oth_date2, self.one_oth_date3]
        members = self.user.members_state
        propositions = []
        while True:
            #print("ok")
            # Random proposition
            proposition = random.choice(self.propositions)
            if proposition not in propositions:
                propositions.append(proposition)
            timer = True
            for element in proposition["Parts"]:
                if element in members.keys() and members[element] == False:
                    #print("if")
                    timer = False
            if timer == True:
                print("here")
                names_but[row].configure(text=proposition["name"])
                date = datetime.date.today().strftime("%d/%m")
                dates[row].configure(text=date, font=("arial",14), text_color=("gray10", "gray10"))
                self.trainings[names_str[row]] = [proposition["name"], date]
                # print("self.training = ", self.trainings)
                self.current_training = proposition
                return proposition
            # End condition
            if len(propositions) == len(self.propositions):
                print("Pas de programmes disponibles.")
                names_but[row].configure(text="----------------------------------------------------------------------------------------------")
                dates[row].configure(text="--/--")
                self.trainings[names_str[row]] = ["----------------------------------------------------------------------------------------------", "--/--"]
                return -1

    def debug(self):
        self.user.display()

# https://pythonguides.com/python-tkinter-frame/

if __name__ == "__main__":

    user1 = Entity("John")
    user2 = Entity("Carla")

    app = App(user=user1)
    app.mainloop()